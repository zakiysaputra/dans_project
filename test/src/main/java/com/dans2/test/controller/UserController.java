package com.dans2.test.controller;

import com.dans2.test.model.User;
import com.dans2.test.model.Job;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class UserController {

    // handler method to handle home page and login request
    @GetMapping("/index")
    public String login(){
        return "index";
    }
    
    // handler method to handle home page and login request
    @RequestMapping("/hello")
    @ResponseBody
    private String hello() {
     return "Hello World";
    }
    
    @RequestMapping("/positions/{id}")
    @ResponseBody
    private String getJobDetails(@PathVariable long id) {
     String uri = "http://dev3.dansmultipro.co.id/api/recruitment/positions/" + id;
     RestTemplate restTemplate = new RestTemplate();
     
     Job job = restTemplate.getForObject(uri, Job.class);
     System.out.println("Job: " + job);
     
     System.out.println("jobid: " + job.getId());
     System.out.println("type: " + job.getType());
     System.out.println("url: " + job.getUrl());
     System.out.println("strDateTime: " + job.getstrDateTime());
     System.out.println("company: " + job.getCompany());
     System.out.println("companyURL: " + job.getCompanyURL());
     System.out.println("location: " + job.getLocation());
     System.out.println("title: " + job.getTitle());
     System.out.println("description: " + job.getDescription());
     System.out.println("how to apply: " + job.getHowToApply());
     System.out.println("company logo: " + job.getCompanyLogo());
     return "User detail page.";
    }
}